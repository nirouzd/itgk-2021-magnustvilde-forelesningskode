''' UKE 34 '''
# Oppsummering kode: Hva ble gjennomgått denne uken?

# interaktiv modus
#Kjør kode direkte, test kode hurtig

# aritmetiske uttrykk
pluss = 9+4
minus = 9-4
gange = 9*4
dele = 9/4
heltallsdivisjon = 9//4
modulo = 9%4

# print-funksjonen
print('Hello World!')
print('2+2: ', 2+2)

# input-funksjonen
a = input('Skriv noe: ')

# variabler
tekst = 'tekst'
tall = 9

# datatyper: string, int, float, boolean
string = 'en tekststreng'
string2 = '4'
integer = 5
flyttall = 5.4
sant = True

# legge sammen variabler, gjøre om mellom datatyper
total = int(string2) + integer
gjorOm = str(total)

# f-string
print(f'2+5 = {2+5}')
variabel = 0.5685343
print(f'{variabel} avrundet: {variabel:.1f}')
print(f'{variabel} i prosent: {variabel:.0%}')
print(f'{variabel} i vitenskapelig notasjon: {variabel:e}')
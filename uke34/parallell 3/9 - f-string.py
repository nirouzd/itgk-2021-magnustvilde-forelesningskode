'''f-string'''

# Vi bygger en f-string!
#navn = input('Skriv inn noe: ')

#print(f'Hei {navn}!')

a = 6
b = 73

print(f'{a} / {b} = {a/b}')
print(f'{a} / {b} = {a/b:.2%}')
print(f'{a} / {b} = {a/b:e}')


'''
# 2 desimaler, f står for float
print(f'{1/3:.2f}')

# 2 desimaler og setter av ti tegn
print(f'{1/3:10.2f}')

# vitenskapelig notasjon på tallet (e)
print(f'{1/3:e}')

# tallet i prosent med 0 desimaler
print(f'{1/3:.0%}')

# heltall der det settes av ti tegn
print(f'{500:10d}')
'''

#Prøv deg frem! Dette læres best av å prøve!

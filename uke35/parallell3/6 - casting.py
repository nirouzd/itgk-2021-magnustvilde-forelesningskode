'''Casting'''

# Hent inn tre tall fra bruker. Gjør om de to første til flyttall, og legg de sammen.
# Legg så sammen det siste tallet foran resultatet av de to tallene, slik at tallet blir mye større

tall1 = float(input('Skriv inn et tall: '))
tall2 = float(input('Skriv inn enda et tall: '))
tall3 = input('Skriv inn et tredje tall: ')
resultat = tall1+tall2
print(resultat)
leggeSammen = tall3 + str(resultat)
leggeSammen2 = f'{tall3}{resultat}'
print(leggeSammen)
print(leggeSammen2)


# still et ja/Nei spørsmål til bruker der bruker skal skrive 'True' eller trykk Enter, og
# gjør resultatet om til en boolean

svar = bool(float(input('Heter du Magnus? Skriv svar som 1 eller skriv 0: ')))
print(svar)

